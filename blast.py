from Bio import Entrez
import docker
import docker.errors
import json
import os

Entrez.email = "ddemingo@xtec.cat"

WORK_DIR = os.getcwd()

BLAST_DB = f"{WORK_DIR}/blast/blastdb"
BLAST_QUERIES = f"{WORK_DIR}/blast/queries"

for dir in [BLAST_DB, BLAST_QUERIES]:
    if not os.path.isdir(dir):
        os.makedirs(dir)

client = docker.from_env()

try:
    blast = client.containers.get("blast")
    if blast.status != "running":
        blast.start()

except docker.errors.NotFound:
    blast = client.containers.run(
        "ncbi/blast",
        "tail -f /dev/null",
        name="blast",
        detach=True,
        volumes=[
            f"{BLAST_DB}:/blast/blastdb",
            f"{BLAST_QUERIES}:/blast/queries",
        ],
    )

def search_protein(id):
    
    __update_db("pdbaa")

    query = __fetch("protein", id)
    command = f"blastp -query {query} -db pdbaa -outfmt 15"
    result = blast.exec_run(command)
    if result.exit_code == 0:
        return json.loads(result.output)
    raise Exception("Exec result {result.exit_code}")


# nuccore, protein
# Returns urllib.error.HTTPError if ...
def __fetch(db: str, id):

    file = "{}/{}.fasta".format(BLAST_QUERIES, id)
    if not os.path.exists(file):
        print(f"Downloading file {id}...")
        with Entrez.efetch(db=db, id=id, rettype="fasta") as response:
            with open(file, "w") as out:
                out.write(response.read())

    return f"/blast/queries/{id}.fasta"


def __update_db(db):
    if not os.path.exists(f"{BLAST_DB}/pdbaa.pdb"):
        print("update pdbaa blastdb")
        output = blast.exec_run(
            "update_blastdb.pl -source gcp pdbaa", workdir="/blast/blastdb"
        )
        print(output)
