import blast
import os
import pytest
import urllib

def test_fetch():
    query = blast.__fetch("protein", "AAA59595.1")
    assert os.path.exists("blast/queries/AAA59595.1.fasta")
    assert query == "/blast/queries/AAA59595.1.fasta"

    with pytest.raises(urllib.error.HTTPError) as error:
        blast.__fetch("protein", "A")

    assert error.value.code == 400

def test_search():
    record = blast.search_protein("AAA59595.1")
    hits = record["BlastOutput2"][0]["report"]["results"]["search"]["hits"]
    assert (len(hits) > 300)
    #print(json.dumps(hits[0], indent=4))
