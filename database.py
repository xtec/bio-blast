import aiofile
import asyncio
import json
from google.cloud import storage
import os

# https://www.ncbi.nlm.nih.gov/IEB/ToolBox/CPP_DOC/lxr/source/src/app/blast/update_blastdb.pl


BLASTDB_PATH = "blast/blastdb" 
BLASTDB_METADATA = "blastdb-metadata-1-1.json"


async def main():

    # share code with blast.py
    if not os.path.exists(BLASTDB_PATH):
        os.makedirs(BLASTDB_PATH)

    dbname = "ref_euk_rep_genomes"

    google(dbname)


# Google Storage
# https://cloud.google.com/python/docs/reference/storage/latest
## https://github.com/GoogleCloudPlatform/python-docs-samples/blob/main/notebooks/rendered/cloud-storage-client-library.md
# https://cloud.google.com/storage/docs/samples/storage-download-file?hl=es-419#storage_download_file-python
def google(dbname):

    client = storage.Client.create_anonymous_client()
    bucket = client.bucket("blast-db")
    metadata = google_blastdb_metadata(bucket)

    db = next(db for db in metadata if db["dbname"] == dbname)
    # print(json.dumps(db, indent=4))
    for file in db["files"]:
        google_download(bucket, file)


def google_blastdb_metadata(bucket):

    latest = bucket.blob("latest-dir").download_as_bytes().decode("utf-8")
    metadata = (
        bucket.blob(f"{latest}/{BLASTDB_METADATA}").download_as_bytes().decode("utf-8")
    )
    metadata = json.loads(metadata)
    return metadata


def google_download(bucket, file):

    blobname = file[14:]
    filename = f"{BLASTDB_PATH}/{os.path.basename(file)}"

    download = True
    metadata = bucket.get_blob(blobname)

    if os.path.exists(filename):
        md5_hash = metadata.md5_hash
        # TODO check hash
        download = False

    if download:
        print(f"{metadata.size}:\t {os.path.basename(file)}")
        bucket.blob(blobname).download_to_filename(filename)

asyncio.run(main())
