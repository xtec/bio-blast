#!/bin/bash

if ! command -v docker &> /dev/null; then
    sudo apt update
    sudo apt install -y docker.io python3-venv
fi

if [ ! -f ".venv/bin/pip" ]; then
    python3 -m venv .venv
fi

./.venv/bin/pip install -r requirements-dev.txt